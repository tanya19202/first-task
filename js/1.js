/*  1) Написать функцию принимающую параметром массив,
 	и возвращающую объект вида { maxValue: ${maxValue}, minValue: ${minValue} }
	Под минимимальным и максимальным значением подрузамевается число.
	 Учесть то что массив может состоять не только из чисел.
*/

 let minMax = (arr) => { 
		let rezultAr = arrOnlyNumber(arr);
		if  (rezultAr.length == 0) {
			console.log('Передайте массив, содержащий числа');
		}
		else {
			let max = Math.max(...arrOnlyNumber(arr)) ;
			let min = Math.min(...arrOnlyNumber(arr));
			console.log({
				maxValue:max,
				minValue:min
		});
		}
		
 }

 let arrOnlyNumber = (arr) => {
	let arrOnlyNumber=[];
	let y=0;
	for (let i = 0; i<arr.length; i++){
	   if (typeof(arr[i])==='number'){
		   arrOnlyNumber[y] = arr[i];
		   y++;
	   } 
	}
	return arrOnlyNumber;
 }

 minMax([5,7,-4,'a',0,'',10e6,false,-17]);
 minMax([]);
// 6) написать пузырьковую сортировку

let sortBubble = (arr) => { 
    let len = arr.length;
    for (let i = 0; i < len ; i++) {
      for(let j = 0 ; j < len - i - 1; j++){ 

      if (arr[j] > arr[j + 1]) {
        let x = arr[j];
        arr[j] = arr[j+1];
        arr[j + 1] = x;
      }
     }
    }
    return arr;
  }

console.log(sortBubble([-7,12,5,0,23,8,-1]));
/*  7) написать рекурсивную функцию выводящую в консоль ряд Фибоначчи. Последнее число
    должно быть меньше 18000.

*/

let s=0;
function fib(n, undefined){
   if((fib.cache[n] === undefined)){
       fib.cache[n] = fib(n-1) + fib(n-2);
   }

   return fib.cache[n];
}
fib.cache = [0, 1, 1];
fib(30);

for(let i=0; i<fib.cache.length;i++){
   (fib.cache[i]<18000)? console.log(fib.cache[i]) : null
}

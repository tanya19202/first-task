/*
   Условие: даны два целочисленных массива. 	
    Задание: объединить два массива в один без повторяющихся элементов. Использовать встроенный в JS объект,
     позволяющий хранить уникальные значения любого типа.

*/
let first = [ 12, -44, 0, 30 ] ;
let second = [ 5, 3, -56 ,12, 30] ;
let full = first.concat(second);
let unique = new Set();
full.forEach( elem => unique.add(elem))
console.log(unique);






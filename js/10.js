/*
      написать функцию принимающую на вход число. Число - высота треуголника.
       Вывести в консоль треугольник из символов '^'.
 
*/

let i = 0;
let triangle = (h) => {
      let space = "", znak = "";
      while (i < h) {
            space = "";
            znak = "";
            for (j = 0; j < h - i; j++) space += " ";
            for (j = 0; j < 2 * i + 1; j++) znak += "*";
            console.log(space + znak);
            i++;
      }
}
triangle(5)
/*  3) дан массив arr = [3, -2, 6, 1, 3, 30, -8, 1]. Выводить в консоль числа пока не встретиться 1. 
    Сдлеать это с помощью циклов while и do while.
*/
console.log('with while:')  ;
let arr = [3, -2, 6, 1, 3, 30, -8, 1];
let i=0;
while( i< arr.length ){
   if(arr[i]!==1)  {
     console.log(arr[i])  ;
     i++;
   }
   else  break
}

i=0;
console.log('with do while:')  ;
do{
    if(arr[i]!==1)  {
      console.log(arr[i])  ;
      i++;
    }
    else  break
}
while( i< arr.length )



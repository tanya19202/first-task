/*
    вывести числа в  консоль от 1 до 20. Числа должны печататься с интервалом 100 миллисекунд.
     Использовать setInterval.
 
*/
let i=1;
let chisla = setInterval(function()
      {
            console.log(i);
            if (i == 20) clearInterval(chisla);
            i++;
            
      }, 100);
